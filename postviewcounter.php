<?php
/*
Plugin Name: Post View Counter
Plugin URI: https://bitbucket.org/mapdigital/wpplugin-postviewcounter
Author: Map Digital
Author URI: http://mapdigital.com.au/
Description: Ajax function to count every single post view and store in database. This function will pass through admin-ajax.php, so this plugin is compatible with WPEngine hosting.
Version: 1.0
*/

/**
 * Post View Counter main plugin class namespace
 *
 * :TODO:
 * Ajax function to count every single post view and store in database
 *
 * @author	Tom Nguyen <nguyenkimtoan@gmail.com>
 * @since	5 Jul 2013
 */
abstract class PostViewCounter
{
	const SCRIPT_VERSION = '1.0';

	public static function init()
	{
		$cls = get_class();

		add_action('wp_enqueue_scripts', array($cls, 'enqueueJS'));
		if (get_option('pvc_autotrack')) {
			add_action('wp_head', array($cls, 'renderTrackingCode'));
		}

		// add action //wp_ajax_nopriv_: for front-end
		//trackView_callback
		add_action('wp_ajax_nopriv_PVC_trackView', array($cls, 'trackView_callback'));
		add_action('wp_ajax_PVC_trackView', array($cls, 'trackView_callback'));

		//getViewCount_callback
		add_action('wp_ajax_nopriv_PVC_getViewCount', array($cls, 'getViewCount_callback'));
		add_action('wp_ajax_PVC_getViewCount', array($cls, 'getViewCount_callback'));

		//getMostViewedPages_callback
		add_action('wp_ajax_nopriv_PVC_getMostViewedPages', array($cls, 'getMostViewedPages_callback'));
		add_action('wp_ajax_PVC_getMostViewedPages', array($cls, 'getMostViewedPages_callback'));

		// add configuration UI
		add_action('admin_menu', array($cls, 'setupAdminScreens'));
		add_action('load-settings_page_post-view-counter', array($cls, 'handleOptions'));
		add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($cls, 'addPluginPageLinks'));

		// initial options
		register_activation_hook(__FILE__, array($cls, 'runInstall'));
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------
	//	Plugin hooks
	//----------------------------------------------------------------------------------------------------------------------------------------------------

	public static function enqueueJS()
	{
		wp_register_script('pvc-js', plugins_url('postviewcounter.js', __FILE__), array('jquery'), self::SCRIPT_VERSION, true);
		wp_enqueue_script('pvc-js');
		wp_localize_script('pvc-js', 'MyAjax', array('ajaxurl' => admin_url('admin-ajax.php' )));
	}

	public static function renderTrackingCode()
	{
		if (!is_single()) {
			return;
		}
		?>
		<script type="text/javascript">
			typeof PostViewCounter != 'undefined' || (PostViewCounter = { toTrack : [] });
			PostViewCounter.toTrack.push(<?php echo get_the_ID(); ?>);
		</script>
		<?php
	}

	public static function trackView_callback()
	{
		echo self::trackView(intval( $_POST['post_id'] ));
		exit(0);
	}

	public static function getViewCount_callback()
	{
		echo self::getViewCount(intval( $_POST['post_id'] ));
		exit(0);
	}

	public static function getMostViewedPages_callback()
	{
		$postTaxonomies = !empty($_POST['post_taxonomies']) ? $_POST['post_taxonomies'] : array();
		$queryArgs = array(
			'post_type' => !empty($_POST['post_types']) ? $_POST['post_types'] : 'any',
		);

		if (count($postTaxonomies) > 0) {
			$tax_query = array('relation' => 'OR');

			foreach ($postTaxonomies as $tax => $terms) {
				$tax_query[] = array(
							'taxonomy' => $tax,
							'field' => 'slug',
							'terms' => $terms,
						);
			}

			$queryArgs['tax_query'] = $tax_query;
		}

		$queryArgs = apply_filters('pvc_mostviewedpages_query_args', $queryArgs);

		echo json_encode(self::getMostViewedPages(intval($_POST['post_limit']), $queryArgs));
		exit(0);
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------
	//	Administration UI
	//----------------------------------------------------------------------------------------------------------------------------------------------------

	public static function setupAdminScreens()
	{
		add_submenu_page('options-general.php',  __('Post View Counter'), __('Post View Counter'), 'manage_options', 'post-view-counter', array(get_class(), 'drawSettingsPage'));
	}

	public static function drawSettingsPage()
	{
		include('pvc-settings-page.php');
	}

	public static function handleOptions()
	{
		if (!empty($_POST)) {
			update_option('pvc_metadata_key', empty($_POST['metadata_key']) ? false : $_POST['metadata_key']);
			update_option('pvc_autotrack', !empty($_POST['autotracking']));

			add_action('admin_notices', array(get_class(), 'handleUpdateNotice'));
		}
	}

	public static function handleUpdateNotice()
	{
		echo '<div class="updated"><p>Settings saved.</p></div>';
	}

	public static function addPluginPageLinks($links)
	{
		array_unshift($links, '<a href="options-general.php?page=post-view-counter">Settings</a>');
		return $links;
	}

	public static function runInstall()
	{
		update_option('pvc_autotrack', 1);	// set defaults
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------
	//	Public API
	//----------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * Track a pageview against a post
	 * @param  int $postID ID of the post being tracked
	 * @return the number of views the post has after this event is tracked
	 */
	public static function trackView($postID = null)
	{
		if (!isset($postID)) {
			$postID = get_the_ID();
		}

		$count = self::getViewCount($postID);
		$count_key = self::getMetadataKey();

		if (!$count) {
			$count = 1;
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, $count);
		} else {
			$count++;
			update_post_meta($postID, $count_key, $count);
		}

		// return post view count after updating
		return $count;
	}

	/**
	 * Get the view count of a post
	 * @param  int $postID ID of the post to retrieve count for
	 * @return the number of views the post has
	 */
	public static function getViewCount($postID)
	{
		return get_post_meta($postID, self::getMetadataKey(), true);
	}

	/**
	 * Get the most viewed pages
	 * @param  integer $postLimit      number of posts to retrieve
	 * @param  array   $queryArgs      any other query arguments to pass in to the WP_Query which loads the results
	 * @return array of post objects
	 */
	public static function getMostViewedPages($postLimit = 0, $queryArgs = array(), $queryDateIfNoResults = true)
	{
		$args = array_merge(array(
			'post_status' => 'publish',
			'posts_per_page' => $postLimit,
			'meta_key' => self::getMetadataKey(),
			'orderby' => 'meta_value_num',
			'order' => 'DESC'
		), $queryArgs);

		$query = new WP_Query($args);

		// if we got nothing, maybe nothing is tracked yet? Pull out the most recent posts instead
		if ($query->found_posts == 0 && $queryDateIfNoResults) {
			unset($args['meta_key']);
			$args['orderby'] = 'date';
			$query = new WP_Query($args);
		}

		// return post view count in 'view_count' member for each post
		$posts = array();

		foreach ($query->posts as $post) {
			$count = get_post_meta($post->ID, self::getMetadataKey(), true);
			if (!$count) {
				$count = 0;
			}
			$post->view_count = $count;
			$posts[] = apply_filters('pvc_mostviewedpages_post_results', $post);
		}

		return $posts;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------
	//	Internals
	//----------------------------------------------------------------------------------------------------------------------------------------------------

	public static function getMetadataKey()
	{
		return get_option('pvc_metadata_key', 'pvc_views');
	}
}
PostViewCounter::init();
