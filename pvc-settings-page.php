<?php
/**
 * Post View Counter admin settings page
 *
 * @author	Sam Pospischil <pospi@spadgos.com>
 * @since	9 Jul 2013
 */

$metaKey = get_option('pvc_metadata_key', 'pvc_views');
$doTracking = get_option('pvc_autotrack');

?>
<h1>Post View Counter</h1>

<form method="post" id="pvc-settings">
<p>
	<label for="pvc-meta-key">
		View count metadata key:
		<input type="text" name="metadata_key" id="pvc-meta-key" value="<?php echo esc_attr($metaKey); ?>" /><br />
		<small>(This determines the internal name to store each post's view count under. You may modify it for compatibility with your theme or existing code.)</small>
	</label>
</p>
<p>
	<label for="pvc-autotracking">
		<input type="checkbox" name="autotracking" id="pvc-autotracking"<?php if ($doTracking) echo ' checked="checked"'; ?> />
		<span>Automatically track views for single pages</span><br />
		<small>(Selecting this option will automatically track any views to single pages. You may disable it if you wish to call the JavaScript tracking API yourself.)</small>
	</label>
</p>
<p>
	<input type="submit" name="save" value="Update settings" />
</p>
</form>
