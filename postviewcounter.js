// exports
typeof PostViewCounter != 'undefined' || (PostViewCounter = { toTrack : [] });

(function($) {

// main API
$.extend(PostViewCounter, {
	trackView: function(postID) {
		var data = {
			action: 'PVC_trackView',
			post_id: postID
		};
		$.ajax({
			url: MyAjax.ajaxurl,
			type: 'POST',
			data: data,
			dataType: "html",
			success: function(response) {
			}
		});
	},
	getViewCount: function(onComplete, postID) {
		var data = {
			action: 'PVC_getViewCount',
			post_id: postID
		};
		$.ajax({
			url: MyAjax.ajaxurl,
			type: 'POST',
			data: data,
			dataType: "html",
			success: function(response) {
				onComplete(response);
			}
		});
	},
	getMostViewedPages: function(onComplete, limit, postTypes, taxonomies, extraData) {
		var data = $.extend({
			action: 'PVC_getMostViewedPages',
			post_limit: limit,
			post_types: postTypes,
			post_taxonomies: taxonomies
		}, extraData || {});
		$.ajax({
			url: MyAjax.ajaxurl,
			type: 'POST',
			data: data,
			dataType: "json",
			success: function(response) {
				onComplete(response);
			}
		});
	}
});

// track any post IDs already declared
$.each(PostViewCounter.toTrack, function(i, postID) {
	PostViewCounter.trackView(postID);
});

// override toTrack push() method to track as pushed from now on
PostViewCounter.toTrack.push = function(postID) {
	PostViewCounter.trackView(postID);
};

})(jQuery);
