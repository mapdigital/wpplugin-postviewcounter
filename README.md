# Post View Counter

Helper plugin for reliably tracking pageviews regardless of caching mechanisms in place on a site.

### Clientside API

	jQuery(document).ready(function($) {
		// track a single pageview
		PostViewCounter.trackView(1825);

		// get number of pageviews for a post
		PostViewCounter.getViewCount(return_ViewCount, 1825);

		// query the most viewed posts
		var taxonomy = {
			category : ['events', 'test'],
			post_tag : ['example']
		};
		PostViewCounter.getMostViewedPages(return_ViewedPages, 9, ["page", "post", "event"], taxonomy);
	});

	//callback function for getViewCount
	function return_ViewCount(count) {
		alert(count);
	}
	//callback function for getMostViewedPages
	function return_ViewedPages(postArray) {
		console.log(postArray);
	}

### Serverside API
(performing same operations as the above)

	PostViewCounter::trackView(1825);

	$count = PostViewCounter::getViewCount(1825);

	$mostViewed = PostViewCounter::getMostViewedPages(9, array(
		'post_type' => array("page", "post", "event"),
		'tax_query' => array(
			'relation' => 'OR',
			array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => array('events', 'test'),
			),
			array(
				'taxonomy' => 'post_tag',
				'field' => 'slug',
				'terms' => 'example',
			),
		),
	));

### Available Hooks & Filters

* `pvc_mostviewedpages_query_args`: Allows filtering the query arguments to be passed to the underlying `WP_Query` object when making a clientside call through `PostViewCounter.getMostViewedPages`. This method is primarily a security feature, since some arguments `WP_Query` accepts may be susceptible to attacks. The best use of this filter is to accept additional POST arguments and modify your query accordingly. For example:

		// Clientside call:

		PostViewCounter.getMostViewedPages(function(posts) {
			console.log(posts);
		}, 5, null, null, {		// :NOTE: passing NULL for post type and taxonomies will make an unfiltered query
			thisDay : 1
		});

		// Serverside code:

		add_filter('pvc_mostviewedpages_query_args', function($args) {
			if (!empty($_POST['thisDay'])) {
				$args['year'] = date('Y');
				$args['monthnum'] = date('m');
				$args['day'] = date('d');
			}

			return $args;
		});

* `pvc_mostviewedpages_post_results`: Allows filtering the post objects returned to the JavaScript API. By default, posts contain all the usual information you would receive when loading them directly from `get_post()`.
